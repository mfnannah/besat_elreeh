package com.bsat_elreeh.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import com.bsat_elreeh.api.Image;
import com.bsat_elreeh.api.Marker;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.List;

public class DB extends DatabaseHandler {

    SQLiteDatabase db;

    public DB(Context c) {
        super(c, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DB open() throws SQLDataException {
        db = getWritableDatabase();
        return this;
    }


    public void close() {
        db.close();
        return;
    }

    // TABLE_GALLERY
    // -------------------------------------------------------------------------------------------------------------------

    public List<Image> getAllGALLERY(String userId, String map_marker_id) {
        List<Image> list = new ArrayList<>();
        String query = "SELECT " +
                colGALLERY._ID + "," +
                colGALLERY.KEY_USER_ID + "," +
                colGALLERY.KEY_MAP_MARKERS_ID + "," +
                colGALLERY.KEY_IMAGE
                + " FROM " + TABLE_GALLERY + " WHERE " + colGALLERY.KEY_USER_ID + "= '" + userId + "'"
                + " AND " + colGALLERY.KEY_MAP_MARKERS_ID + "= '" + map_marker_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst()) {
            do {
                Image image = new Image(c.getInt(c.getColumnIndex(colGALLERY._ID)),
                        c.getString(c.getColumnIndex(colGALLERY.KEY_MAP_MARKERS_ID)),
                        c.getString(c.getColumnIndex(colGALLERY.KEY_USER_ID)),
                        c.getString(c.getColumnIndex(colGALLERY.KEY_IMAGE)));
                list.add(image);
            } while (c.moveToNext());
        }
        c.close();

        return list;
    }

    public Long insertGALLERY(int id, String userID, int mapMarkersID, String image) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colGALLERY._ID, id);
        val.put(colGALLERY.KEY_USER_ID, userID);
        val.put(colGALLERY.KEY_MAP_MARKERS_ID, mapMarkersID);
        val.put(colGALLERY.KEY_IMAGE, image);

        return db.insert(TABLE_GALLERY, null, val);
    }

    public boolean deleteGALLERY(int id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_GALLERY, colGALLERY._ID + "=?", new String[]{String.valueOf(id)}) > 0;
    }

    public void updateGALLERY(int id, String userID, int mapMarkersID, String image) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colGALLERY._ID, id);
        val.put(colGALLERY.KEY_USER_ID, userID);
        val.put(colGALLERY.KEY_MAP_MARKERS_ID, mapMarkersID);
        val.put(colGALLERY.KEY_IMAGE, image);

        db.update(TABLE_GALLERY, val, colGALLERY._ID + "=?", new String[]{String.valueOf(id)});
        Log.e("UPDATED_GALLERYS", db.update(TABLE_GALLERY, val, colGALLERY._ID + "=?", new String[]{String.valueOf(id)}) + "");
    }

    public static abstract class colGALLERY implements BaseColumns {
        public static final String KEY_USER_ID = "user_id";
        public static final String KEY_MAP_MARKERS_ID = "map_marker_id";
        public static final String KEY_IMAGE = "image";
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // TABLE_MAP_MARKERS
    // -------------------------------------------------------------------------------------------------------------------

    public List<Marker> getAllMAP_MARKERS() {
        List<Marker> list = new ArrayList<>();
        String query = "SELECT " +
                colMAP_MARKERS._ID + "," +
                colMAP_MARKERS.KEY_NAME + "," +
                colMAP_MARKERS.KEY_LATITUDE + "," +
                colMAP_MARKERS.KEY_LONGITUDE + "," +
                colMAP_MARKERS.KEY_RATE
                + " FROM " + TABLE_MAP_MARKERS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst()) {
            do {
                Marker marker = new Marker(c.getInt(c.getColumnIndex(colMAP_MARKERS._ID)),
                        c.getString(c.getColumnIndex(colMAP_MARKERS.KEY_LATITUDE)),
                        c.getString(c.getColumnIndex(colMAP_MARKERS.KEY_LONGITUDE)),
                        c.getString(c.getColumnIndex(colMAP_MARKERS.KEY_NAME)),
                        c.getString(c.getColumnIndex(colMAP_MARKERS.KEY_RATE)));
                list.add(marker);
            } while (c.moveToNext());
        }
        c.close();

        return list;
    }

    public List<Marker> getMAP_MARKERS(int id) {
        List<Marker> list = new ArrayList<>();

        String query = "SELECT " +
                colMAP_MARKERS._ID + "," +
                colMAP_MARKERS.KEY_NAME + "," +
                colMAP_MARKERS.KEY_LATITUDE + "," +
                colMAP_MARKERS.KEY_LONGITUDE + "," +
                colMAP_MARKERS.KEY_RATE
                + " FROM " + TABLE_MAP_MARKERS + " WHERE " + colMAP_MARKERS._ID + "= " + id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst()) {
            do {
                Marker marker = new Marker(c.getInt(c.getColumnIndex(colMAP_MARKERS._ID)),
                        c.getString(c.getColumnIndex(colMAP_MARKERS.KEY_LATITUDE)),
                        c.getString(c.getColumnIndex(colMAP_MARKERS.KEY_LONGITUDE)),
                        c.getString(c.getColumnIndex(colMAP_MARKERS.KEY_NAME)),
                        c.getString(c.getColumnIndex(colMAP_MARKERS.KEY_RATE)));
                list.add(marker);
            } while (c.moveToNext());
        }


        c.close();
        return list;

    }

    public int getMAP_MARKERS_ID(double latitude, double longitude) {
        int map_markerID = 0;

        String query = "SELECT " +
                colMAP_MARKERS._ID + "," +
                colMAP_MARKERS.KEY_NAME + "," +
                colMAP_MARKERS.KEY_LATITUDE + "," +
                colMAP_MARKERS.KEY_LONGITUDE + "," +
                colMAP_MARKERS.KEY_RATE
                + " FROM " + TABLE_MAP_MARKERS + " WHERE " + colMAP_MARKERS.KEY_LATITUDE + "= '" + latitude + "'"
                + " AND " + colMAP_MARKERS.KEY_LONGITUDE + "= '" + longitude + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst()) {
            do {
                map_markerID = c.getInt(c.getColumnIndex(colMAP_MARKERS._ID));
            } while (c.moveToNext());
        }

        c.close();
        return map_markerID;
    }

    public Long insertMAP_MARKERS(int id, String name, String latitude, String longitude, double rate) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colMAP_MARKERS._ID, id);
        val.put(colMAP_MARKERS.KEY_NAME, name);
        val.put(colMAP_MARKERS.KEY_LATITUDE, latitude);
        val.put(colMAP_MARKERS.KEY_LONGITUDE, longitude);
        val.put(colMAP_MARKERS.KEY_RATE, rate);

        return db.insert(TABLE_MAP_MARKERS, null, val);
    }

    public boolean deleteMAP_MARKERS(int id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_MAP_MARKERS, colMAP_MARKERS._ID + "=?", new String[]{String.valueOf(id)}) > 0;
    }

    public void updateMAP_MARKERS(int id, String name, String latitude, String longitude, double rate) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(colMAP_MARKERS._ID, id);
        val.put(colMAP_MARKERS.KEY_NAME, name);
        val.put(colMAP_MARKERS.KEY_LATITUDE, latitude);
        val.put(colMAP_MARKERS.KEY_LONGITUDE, longitude);
        val.put(colMAP_MARKERS.KEY_RATE, rate);
        db.update(TABLE_MAP_MARKERS, val, colMAP_MARKERS._ID + "=?", new String[]{String.valueOf(id)});
    }

    public static abstract class colMAP_MARKERS implements BaseColumns {
        public static final String KEY_NAME = "name";
        public static final String KEY_LATITUDE = "latitude";
        public static final String KEY_LONGITUDE = "longitude";
        public static final String KEY_RATE = "rate";
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


}
