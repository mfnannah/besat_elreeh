package com.bsat_elreeh;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bsat_elreeh.DataBase.DB;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Created by Ahmed J AbuSada on 30/8/2015.
public class AddMarkerDialogFragment extends DialogFragment {
    private static final String URL_ADD_MARKER = "http://smart-solution.co/web/demo/json/markers/add";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
    TextView confirm, dismiss;
    double longitude, latitude;
    EditText location_name;
    RatingBar ratingBar;
    DB db;
    GoogleMap mMap;
    List<com.bsat_elreeh.api.Marker> listMarkers = new ArrayList<>();

    public GoogleMap getmMap() {
        return mMap;
    }

    public void setmMap(GoogleMap mMap) {
        this.mMap = mMap;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        db.close();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.add_marker_dialogfragment, null);
        db = new DB(getActivity());
        try {
            db.open();
        } catch (SQLDataException e) {
            e.printStackTrace();
        }

        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        ratingBar.setNumStars(10);

        longitude = getArguments().getDouble(LONGITUDE, 0.0);
        latitude = getArguments().getDouble(LATITUDE, 0.0);
        location_name = (EditText) view.findViewById(R.id.location_name);
        confirm = (TextView) view.findViewById(R.id.confirm);
        dismiss = (TextView) view.findViewById(R.id.dismiss);


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = location_name.getText().toString();
                if (name.length() > 0 && ratingBar.getRating() > 0.0) {
                    postMarker(name, ratingBar.getRating() + "", latitude + "", longitude + "");
                    dismiss();
                } else {
                    if (name.length() == 0)
                        location_name.setError(getString(R.string.This_field_can_not_be_blank));
                }
            }
        });

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        builder.setView(view);
        return builder.create();
    }

   public void postMarker(String name, String rate, String latitude, String longitude) {
        final Map<String, String> params = new HashMap<>();
        try {
            params.put("name", name);
            params.put("rate", rate);
            params.put("latitude", latitude);
            params.put("longitude", longitude);
        } catch (Exception e) {
            e.getMessage();
        }
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, URL_ADD_MARKER, new JSONObject(params)
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.w("RES", response + "");
                    Log.e("in", "");
                    Log.w("RESUCCESS", response + "");
                    JSONObject responses = response.getJSONObject("response");
                    String status = responses.getString("status");
                    String message = responses.getString("message");

                    if (status.equals("success")) {
                        JSONObject marker = response.getJSONObject("marker");
                        int id = marker.getInt("id");
                        String name = marker.getString("name");
                        String lat = marker.getString("latitude");
                        String long_ = marker.getString("longitude");
                        double rate = marker.getDouble("rate");
                        db.insertMAP_MARKERS(id, name, lat, long_, rate);
                    } else
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("out", "");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("first ERROR", error.getMessage() + ":");
                error.printStackTrace();
                Log.e("error", "");
                Toast.makeText(getActivity(), "invalid", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        MySingleton.getInstance(getActivity()).addToRequestQueue(customRequest);
    }

}