package com.bsat_elreeh;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bsat_elreeh.DataBase.DB;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {
    private static final String URL_SHARE_MARKERS = "http://smart-solution.co/web/demo/json/trip/share";
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    public static final String USER_ID = "user_id";
    private ProgressDialog pDialog;
    GoogleMap mMap;
    GPSTracker gps;
    Marker m = null;
    FloatingActionButton fab;
    GoogleApiClient client;
    NavigationView navigationView;
    String androidId;
    boolean canAddMarker = false, toShare = false;
    List<com.bsat_elreeh.api.Marker> listMarkers = new ArrayList<>();
    List<Marker> listMarker = new ArrayList<>();
    List<Integer> listMarkerId = new ArrayList<>();
    DB db;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.font))
                .setFontAttrId(R.attr.fontPath)
                .build());
        db = new DB(this);
        try {
            db.open();
        } catch (SQLDataException e) {
            e.printStackTrace();
        }

        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getString(R.string.please_wait));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences prefs = getSharedPreferences(MainActivity.MY_PREFS_NAME, Context.MODE_PRIVATE);
        androidId = prefs.getString(MainActivity.USER_ID, "null");

        MapFragment mapFragment = MapFragment.newInstance();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.container, mapFragment);
        fragmentTransaction.commit();
        mapFragment.getMapAsync(this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!canAddMarker) {
                    fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
                    canAddMarker = true;
                    m = null;
                } else {
                    fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_add));
                    if (m != null) {
                        AddMarkerDialogFragment fragment = new AddMarkerDialogFragment();
                        fragment.setmMap(mMap);
                        Bundle bundle = new Bundle();
                        bundle.putDouble(AddMarkerDialogFragment.LATITUDE, m.getPosition().latitude);
                        bundle.putDouble(AddMarkerDialogFragment.LONGITUDE, m.getPosition().longitude);
                        fragment.setArguments(bundle);
                        fragment.show(getSupportFragmentManager(), "blur_sample");
                    }
                    canAddMarker = false;
                    mMap.clear();
                    listMarkers.addAll(db.getAllMAP_MARKERS());
                    for (com.bsat_elreeh.api.Marker marker : listMarkers) {
                        double lat = Double.parseDouble(marker.getLatitude());
                        double long_ = Double.parseDouble(marker.getLongitude());
                        LatLng yourLocation = new LatLng(lat, long_);
                        mMap.addMarker(new MarkerOptions().position(yourLocation).title(marker.getName())
                                .snippet(getString(R.string.rate) + " " + marker.getRate()));
                    }
                    getMyLocation(gps);
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        int[][] states = new int[][]{
                new int[]{android.R.attr.state_enabled}, // enabled
                new int[]{-android.R.attr.state_enabled}, // disabled
                new int[]{-android.R.attr.state_checked}, // unchecked
                new int[]{android.R.attr.state_pressed}  // pressed
        };

        int[] colors = new int[]{
                Color.WHITE,
                Color.WHITE,
                Color.WHITE,
                Color.WHITE
        };

        ColorStateList myList = new ColorStateList(states, colors);
        navigationView.setItemTextColor(myList);

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
            if (!toShare) {
                Toast.makeText(getApplicationContext(), getString(R.string.check_marker), Toast.LENGTH_SHORT).show();
                item.setIcon(getResources().getDrawable(R.drawable.ic_check));
                toShare = true;
            } else {
                item.setIcon(getResources().getDrawable(R.drawable.ic_share));
                toShare = false;
                Log.e("SIZE", listMarker.size() + "");
                listMarkerId.clear();
                for (Marker marker : listMarker) {
                    int map_marker_id = db.getMAP_MARKERS_ID(marker.getPosition().latitude, marker.getPosition().longitude);
                    listMarkerId.add(map_marker_id);
                    Log.e("IDDD", map_marker_id + "");
                    marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }

                if (listMarkerId.size() > 0)
                    postShareDate(androidId, listMarkerId);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            // Handle the camera action
        } else if (id == R.id.services) {

        } else if (id == R.id.share_trip) {

        } else if (id == R.id.help) {

        } else if (id == R.id.about) {

        } else if (id == R.id.logout) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (canAddMarker) {
                    if (m != null) { //if marker exists (not null or whatever)
                        m.setPosition(latLng);
                    } else {
                        m = mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
                    }
                }
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (!toShare) {
                    int map_marker_id = db.getMAP_MARKERS_ID(marker.getPosition().latitude, marker.getPosition().longitude);
                    Intent intent = new Intent(MainActivity.this, GalleryActivity.class);
                    intent.putExtra(GalleryActivity.MARKER_ID, map_marker_id);
                    startActivity(intent);
                } else if (canAddMarker) {
                    m.remove();
                } else if (toShare) {
                    marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    listMarker.add(marker);
//                    Log.e("MM", marker.getPosition().latitude+"/"+marker.getPosition().longitude);
                }
                return false;
            }
        });

        gps = new GPSTracker(MainActivity.this);
        getMyLocation(gps);

    }

    public void getMyLocation(GPSTracker gps) {
// check if GPS enabled
        if (gps.canGetLocation()) {

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = gcd.getFromLocation(latitude, longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addresses != null && addresses.size() > 0)
                Log.e("CITY", addresses.get(0).getLocality());

            listMarkers.addAll(db.getAllMAP_MARKERS());
            for (com.bsat_elreeh.api.Marker marker : listMarkers) {
                double lat = Double.parseDouble(marker.getLatitude());
                double long_ = Double.parseDouble(marker.getLongitude());
                LatLng yourLocation = new LatLng(lat, long_);
                mMap.addMarker(new MarkerOptions().position(yourLocation).title(marker.getName()).snippet(getString(R.string.rate) + " " +
                        marker.getRate()));
            }

            // Add a marker in Sydney and move the camera
            LatLng yourLocation = new LatLng(latitude, longitude);
//            mMap.addMarker(new MarkerOptions().position(yourLocation).title(getString(R.string.your_is_here)));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(yourLocation, 15));
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    public void postShareDate(String userid, List<Integer> listMarkerId) {
        showpDialog();
        final Map<String, String> params = new HashMap<>();
        int i = 0;
        try {
            params.put("udid", userid);
            params.put("count", listMarkerId.size() + "");
            for (int marker : listMarkerId) {
                params.put("marker_" + (i++), marker + "");
            }
        } catch (Exception e) {
            e.getMessage();
        }
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, URL_SHARE_MARKERS, new JSONObject(params)
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("RES", response + "");
                    Log.e("in", "");
                    Log.e("RESUCCESS", response + "");
                    JSONObject responses = response.getJSONObject("response");
                    String status = responses.getString("status");
                    String message = responses.getString("message");

                    if (status.equals("success")) {
                        String image = response.getString("image");
                        Log.e("IMAGE", image);
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, image);
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                    }
                    hidepDialog();
                } catch (Exception e) {
                    Log.e("out", "");
                    hidepDialog();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hidepDialog();
                Log.e("first ERROR", error.getMessage() + ":");
                error.printStackTrace();
                Log.e("error", "");
                Toast.makeText(getApplicationContext(), "invalid", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        MySingleton.getInstance(MainActivity.this).addToRequestQueue(customRequest);
    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
