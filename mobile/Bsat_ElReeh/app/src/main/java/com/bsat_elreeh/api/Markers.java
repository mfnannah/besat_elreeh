
package com.bsat_elreeh.api;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Markers {

    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("markers")
    @Expose
    private List<Marker> markers = new ArrayList<Marker>();

    /**
     * 
     * @return
     *     The response
     */
    public Response getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The markers
     */
    public List<Marker> getMarkers() {
        return markers;
    }

    /**
     * 
     * @param markers
     *     The markers
     */
    public void setMarkers(List<Marker> markers) {
        this.markers = markers;
    }

}
