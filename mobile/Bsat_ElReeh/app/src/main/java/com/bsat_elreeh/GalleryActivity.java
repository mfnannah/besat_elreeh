package com.bsat_elreeh;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bsat_elreeh.DataBase.DB;
import com.bsat_elreeh.api.Image;
import com.bsat_elreeh.api.ImagesGallery;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class GalleryActivity extends AppCompatActivity implements ImageChooserListener {
    private static final int MY_SOCKET_TIMEOUT_MS = 200000;
    private GridView gridView;
    private GridAdapter adapter;
    List<Image> list = new ArrayList<>();
    private ImageChooserManager imageChooserManager;
    private ProgressDialog pDialog;
    String mediaPath;
    int chooserType, marker_id;
    public String path;
    String androidId;
    private static final String URL = "http://smart-solution.co/web/demo/json/gallary/images";
    private static final String URL_ = "http://smart-solution.co/web/demo/json/gallary/image";
    public static final String MARKER_ID = "marker_id";
    LinearLayout linear;
    Button take_picture, choose_picture;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    DB db;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.font))
                .setFontAttrId(R.attr.fontPath)
                .build());
        db = new DB(this);
        try {
            db.open();
        } catch (SQLDataException e) {
            e.printStackTrace();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SharedPreferences prefs = getSharedPreferences(MainActivity.MY_PREFS_NAME, Context.MODE_PRIVATE);
        androidId = prefs.getString(MainActivity.USER_ID, "null");
        if (getIntent().hasExtra(MARKER_ID)) {
            marker_id = getIntent().getIntExtra(MARKER_ID, 0);
        }

        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getString(R.string.please_wait));

        gridView = (GridView) findViewById(R.id.gridview);
        getFromAPI(androidId, marker_id);

        linear = (LinearLayout) findViewById(R.id.linear);
        take_picture = (Button) findViewById(R.id.take_picture);
        take_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
            }
        });
        choose_picture = (Button) findViewById(R.id.choose_picture);
        choose_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });

    }

    public void getFromAPI(final String udid, final int markerid) {
        JsonObjectRequest jsonrequest = new JsonObjectRequest(Request.Method.GET, URL + "/" + udid + "/" + markerid, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("in", "");
                            Log.e("RESPONSE", response + "");
                            JSONObject responses = response.getJSONObject("response");
                            String status = responses.getString("status");
                            String message = responses.getString("message");
                            if (status.equals("success")) {
                                GsonBuilder gsonBuilder = new GsonBuilder();
                                Gson gson = gsonBuilder.create();
                                ImagesGallery imagesGallery = gson.fromJson(response.toString(), ImagesGallery.class);
                                for (Image image : imagesGallery.getImages()) {
                                    db.deleteGALLERY(image.getId());
                                    db.insertGALLERY(image.getId(), image.getUserId(), Integer.parseInt(image.getMapMarkersId()),
                                            image.getImagePath());
                                }
                            } else
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        } catch (Exception ex) {
                            Log.e("Fragment catch", "Error: " + ex.getMessage());
                            Log.e("out", "");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Fragment", "Error: " + error.getMessage());
                Log.e("error", "");
                error.printStackTrace();
            }
        });

        RequestQueue queue = Volley.newRequestQueue(GalleryActivity.this);
        queue.add(jsonrequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                list.addAll(db.getAllGALLERY(udid, markerid + ""));
                adapter = new GridAdapter(getApplicationContext(), list);
                adapter.notifyDataSetChanged();
                gridView.setAdapter(adapter);
            }
        });
    }

    Bitmap ShrinkBitmap(String file, int width, int height) {

        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        Bitmap bitmap;

        int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight / (float) height);
        int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth / (float) width);

        if (heightRatio > 1 || widthRatio > 1) {
            if (heightRatio > widthRatio) {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }

        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        return bitmap;
    }

    private void takePicture() {
        chooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_CAPTURE_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        try {
            mediaPath = imageChooserManager.choose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void chooseImage() {
        chooserType = ChooserType.REQUEST_PICK_PICTURE;
        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        try {
            mediaPath = imageChooserManager.choose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("media_path")) {
                mediaPath = savedInstanceState.getString("media_path");
            }
            if (savedInstanceState.containsKey("chooser_type")) {
                chooserType = savedInstanceState.getInt("chooser_type");
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("On Activity Result", requestCode + "");
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (imageChooserManager == null) {
                imageChooserManager = new ImageChooserManager(this, requestCode, true);
                imageChooserManager.setImageChooserListener(this);
                imageChooserManager.reinitialize(mediaPath);
            }
            imageChooserManager.submit(requestCode, data);
        }
    }

    @Override
    public void onImageChosen(final ChosenImage image) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (image != null) {
                    setPath(image.getFilePathOriginal());
                    postImage(new File(image.getFileThumbnail()).toString(), androidId, marker_id);
//                    profile_pic.setImageURI(Uri.parse(new File(image.getFileThumbnail()).toString()));
                }
            }
        });
    }

    @Override
    public void onError(String s) {

    }

    public void postImage(String image, String userid, int markerid) {
        final Map<String, String> params = new HashMap<>();
        try {
            Bitmap bm = ShrinkBitmap(image, 200, 200);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            String ext = getPath().substring(getPath().lastIndexOf("."));
            String type;
            switch (ext) {
                case ".jpeg":
                    bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
                    type = "data:image/jpeg;base64,";
                    break;
                case ".jpg":
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    type = "data:image/jpeg;base64,";
                    break;
                default:
                    Toast.makeText(this, getString(R.string.make_sure_extension), Toast.LENGTH_LONG).show();
                    return;
            }
            //bm is the bitmap object
            byte[] byteArrayImage = baos.toByteArray();
            String encodedImage = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);
            Log.w("BASE64", encodedImage);
            params.put("image", type + encodedImage);
            params.put("userid", userid);
            params.put("markerid", markerid + "");
        } catch (Exception e) {
            e.getMessage();
        }
        showpDialog();
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, URL_, new JSONObject(params)
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.w("RES", response + "");
                    Log.e("in", "");
                    Log.w("RESUCCESS", response + "");
                    String status = response.getString("status");
                    String message = response.getString("message");

                    String user_id = null;
                    int map_marker_id = 0;

                    if (status.equals("success")) {
                        JSONObject images = response.getJSONObject("images");
                        int id = images.getInt("id");
                        user_id = images.getString("user_id");
                        map_marker_id = images.getInt("map_markers_id");
                        String image_path = images.getString("image_path");
                        db.insertGALLERY(id, user_id, map_marker_id, image_path);

                    } else
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    hidepDialog();

                    list.addAll(db.getAllGALLERY(user_id, map_marker_id + ""));
                    adapter = new GridAdapter(getApplicationContext(), list);
                    adapter.notifyDataSetChanged();
                    gridView.setAdapter(adapter);
                } catch (Exception e) {
                    hidepDialog();
                    Log.e("out", "");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("first ERROR", error.getMessage() + ":");
                error.printStackTrace();
                Log.e("error", "");
                hidepDialog();
                Toast.makeText(getApplicationContext(), "invalid", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        MySingleton.getInstance(GalleryActivity.this).addToRequestQueue(customRequest);
        customRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
