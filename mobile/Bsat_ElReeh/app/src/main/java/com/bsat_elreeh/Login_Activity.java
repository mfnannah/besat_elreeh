package com.bsat_elreeh;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bsat_elreeh.DataBase.DB;
import com.bsat_elreeh.api.Marker;
import com.bsat_elreeh.api.Markers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.sql.SQLDataException;
import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Login_Activity extends AppCompatActivity {
    Button skip;
    LinearLayout root;
    private static final String URL = "http://smart-solution.co/web/demo/json/markers";
    private static final String URL_ADD_USER = "http://smart-solution.co/web/demo/json/users/add";
    DB db;
    String android_id;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.font))
                .setFontAttrId(R.attr.fontPath)
                .build());
        db = new DB(this);
        try {
            db.open();
        } catch (SQLDataException e) {
            e.printStackTrace();
        }

        android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e("ANDROID_ID", android_id);
        SharedPreferences.Editor editor = getSharedPreferences(MainActivity.MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(MainActivity.USER_ID, android_id);
        editor.apply();
        getFromAPI();
    }

    public void getFromAPI() {
        JsonObjectRequest jsonrequest = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("in", "");
                    Log.e("RESPONSE", response + "");
                    String status = response.getString("status");
                    String message = response.getString("message");
                    if (status.equals("success")) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        Markers markers = gson.fromJson(response.toString(), Markers.class);
                        for (Marker marker : markers.getMarkers()){
                            db.deleteMAP_MARKERS(marker.getId());
                            db.insertMAP_MARKERS(marker.getId(), marker.getName(), marker.getLatitude(),
                                    marker.getLongitude(), Double.parseDouble(marker.getRate()));
                        }

                    }


                } catch (Exception ex) {
                    Log.e("Fragment", "Error: " + ex.getMessage());
                    Log.e("out", "");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Fragment", "Error: " + error.getMessage());
                Log.e("error", "");
                error.printStackTrace();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(Login_Activity.this);
        queue.add(jsonrequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                postUSER(android_id);
                root = (LinearLayout) findViewById(R.id.root);
                assert root != null;
                skip = (Button) root.findViewById(R.id.skip);
                skip.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Login_Activity.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    public void postUSER(String userid) {
        final Map<String, String> params = new HashMap<>();
        try {
            params.put("udid", userid);
        } catch (Exception e) {
            e.getMessage();
        }
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, URL_ADD_USER, new JSONObject(params)
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.w("RES", response + "");
                    Log.e("in", "");
                    Log.w("RESUCCESS", response + "");
                    JSONObject responses = response.getJSONObject("response");
                    String status = responses.getString("status");
                    String message = responses.getString("message");

                    if (status.equals("success")) {
                        Intent i = new Intent(Login_Activity.this, Login_Activity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }

                } catch (Exception e) {
                    Log.e("out", "");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("first ERROR", error.getMessage() + ":");
                error.printStackTrace();
                Log.e("error", "");
                Toast.makeText(getApplicationContext(), "invalid", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        MySingleton.getInstance(Login_Activity.this).addToRequestQueue(customRequest);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
