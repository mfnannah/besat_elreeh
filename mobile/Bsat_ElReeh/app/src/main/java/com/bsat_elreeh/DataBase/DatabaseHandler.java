package com.bsat_elreeh.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

    // Database Version
    public static final int DATABASE_VERSION = 1;

    // Database Name
    public static final String DATABASE_NAME = "tawgehy";

    // tables name
    public static final String TABLE_GALLERY = "gallery";
    public static final String TABLE_MAP_MARKERS = "map_markers";

    // --------------------------------------------------------
    public interface DbCallback {
        public void onCreate(SQLiteDatabase db);

        public void onUpgrade(SQLiteDatabase db);
    }

    Context context;

    public DatabaseHandler(Context context, String name, CursorFactory cursor,
                           int version) {
        super(context, DATABASE_NAME, cursor, DATABASE_VERSION);
        this.context = context;
    }

    // ---------------------------------------------------------
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_GALLERY_TABLE = "CREATE TABLE " + TABLE_GALLERY + "( "
                + DB.colGALLERY._ID + " INTEGER PRIMARY KEY, "
                + DB.colGALLERY.KEY_USER_ID + " VARCHAR(255), "
                + DB.colGALLERY.KEY_MAP_MARKERS_ID + " LONG REFERENCES " + TABLE_MAP_MARKERS + "(" + DB.colMAP_MARKERS._ID + "),"
                + DB.colGALLERY.KEY_IMAGE + " TEXT)";
        db.execSQL(CREATE_GALLERY_TABLE);

        String CREATE_MAP_MARKERS_TABLE = "CREATE TABLE " + TABLE_MAP_MARKERS + "( "
                + DB.colMAP_MARKERS._ID + " INTEGER PRIMARY KEY, "
                + DB.colMAP_MARKERS.KEY_NAME + " VARCHAR(255), "
                + DB.colMAP_MARKERS.KEY_LATITUDE + " VARCHAR(255), "
                + DB.colMAP_MARKERS.KEY_LONGITUDE + " VARCHAR(255),"
                + DB.colMAP_MARKERS.KEY_RATE + " DOUBLE)";
        db.execSQL(CREATE_MAP_MARKERS_TABLE);

     }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GALLERY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MAP_MARKERS);
        // Create tables again
        onCreate(db);
    }


}
