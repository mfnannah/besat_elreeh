package com.bsat_elreeh;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bsat_elreeh.api.Image;

import java.util.List;

/**
 * Created by Ahmed on 25/6/2015.
 */
public class GridAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    List<Image> list = null;

    public GridAdapter(Context c, List<Image> list) {
        context = c;
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view;

        if (convertView == null) {
//            view = new View(context);
            // get layout from mobile.xml
            view = inflater.inflate(R.layout.item_gallery, null);
            Image image = list.get(position);
            ImageLoader mImageLoader = MySingleton.getInstance(context).getImageLoader();
            NetworkImageView mNetworkImageView = (NetworkImageView) view.findViewById(R.id.img);
            mNetworkImageView.setImageUrl(image.getImagePath(), mImageLoader);
//            ImageView mNetworkImageView = (ImageView) view.findViewById(R.id.img);
//            mNetworkImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.splash));
        } else {
            view = (View) convertView;
        }

        return view;
    }

}





