<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mapmarker extends Model
{
    use SoftDeletes;
    protected $table = 'map_markers';
    public $timestamps = true;
    protected $fillable = ['latitude', 'longitude', 'name', 'rate'];

}