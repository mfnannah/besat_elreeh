<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
    use SoftDeletes;
    protected $table = 'galleries';
    public $timestamps = true;
    protected $fillable = ['map_markers_id', 'user_id', 'image_path'];

}