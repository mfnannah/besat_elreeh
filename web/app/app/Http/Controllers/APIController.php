<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class APIController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth'); 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * this function to git all map markers
     */
    public function getmarkers()
    {
        $markers = \App\Mapmarker::all();
        return  json_encode(array('response' => array('code'=> 200 ,'status' => 'success' ,'message'=>'success' ), 'markers' => $markers));
    }

    /**
     * add new marker
     */
    public function add_new_marker(){
        $latitude = \Input::get('latitude');
        $longitude = \Input::get('longitude');
        $name = \Input::get('name');
        $rate = \Input::get('rate');
        if (!empty($latitude) && !empty($longitude)) {
            $marker_record = \App\Mapmarker::create([
                'latitude' =>$latitude,        
                'longitude' => $longitude,        
                'name' => $name,        
                'rate' => $rate,        
            ]);    
            $marker_record->user_id = $this->get_user_udid($marker_record->user_id);        
            return  json_encode(array('response' => array('code'=> 200 ,'status' => 'success','message'=>'success' ), 'marker' => $marker_record));
        }else{
            return  json_encode(array('response' => array('code'=> 500 ,'status' => 'error','message'=>'Error , enter valid data' )));
        }
    }

    /**
     * this function get user gallary images
     */
    public function image_gallary($udid,$markerid)
    {
        // $headers = apache_request_headers();    
        // return  json_encode($headers);
        if (isset($udid) && !empty($udid) && isset($markerid) && !empty($markerid) ) {
            $udid = $udid;
            $user_id = $this->get_user_id($udid);
            $marker_id = $markerid;
            if (intval($user_id)>0 && intval($marker_id) > 0) {

                $images = \App\Gallery::where('user_id',$user_id)->where('map_markers_id',$marker_id)->get();
                foreach ($images as $row) {
                   $row->image_path = $this->get_full_path('/uploads/gallary/',$row->image_path); 
                   $row->user_id = $this->get_user_udid($row->user_id);
                }
                return  json_encode(array('response' => array('code'=> 200 ,'status' => 'success','message'=>'success'  ), 'images' => $images));
            }else{
                return  json_encode(array('response' => array('code'=> 500 ,'status' => 'error','message' => 'Error , user id or marker id not found' )));
            }  
        }else{
            return  json_encode(array('response' => array('code'=> 500 ,'status' => 'error','message' => 'Error , enter valid udid and marker id' )));
        }                 
    }

    /**
     * add new image to gallary
     */
    public function add_image_to_gallary(){        
        if(!empty(\Input::get('image'))){
            $image = $this->base64_to_jpeg(\Input::get('image'));               
        }else{
            $image = '';
        }
        $user_id = $this->get_user_id(\Input::get('userid'));
        $marker_id = \Input::get('markerid');
        if (!empty($image) && !empty($user_id) && !empty($marker_id)) {
            $image_record = \App\Gallery::create([
                'map_markers_id' =>$marker_id,        
                'user_id' => $user_id,        
                'image_path' => $image,        
            ]);
            $image_record->image_path = $this->get_full_path('/uploads/gallary/',$image_record->image_path);             
            return  json_encode(array('response' => array('code'=> 200 ,'status' => 'success','message'=>'success'  ), 'image' => $image_record));
        }else{
            return  json_encode(array('response' => array('code'=> 500 ,'status' => 'error','message' => 'Error , enter valid data' )));
        }
    }

    /**
     * share trip phots in an one image as grip
     */
    public function shear_trip(){ 
        
        /**
         * select images array from database for udid  and markers
         */
        try{
            $user_id = $this->get_user_id(\Input::get('udid'));
            if (!is_dir('temp/'.$user_id.'/')) {
                @mkdir('temp/'.$user_id.'/', 0777, true);
            }else{
                $files = glob('temp/'.$user_id.'/*'); // get all file names
                foreach($files as $file){ // iterate files
                  if(is_file($file))
                    @unlink($file); // delete file
                }
            }
            // $user_id = \Input::get('udid');
            $marker_ids = array();
            $count = \Input::get('count');
            for ($i=0; $i < intval($count) ; $i++) { 
               $marker_ids[] =  \Input::get('marker_'.$i);
            }
            // return json_encode(\Input::all());
            // $marker_ids = \Input::get('marker');
            $images_array = array();
            if (intval($user_id) > 0 && count($marker_ids) > 0) {
                foreach ($marker_ids as $key => $value) {
                    $marker_ids[$key] = intval($value);
                }
                $images = \App\Gallery::where('user_id',$user_id)->whereIn('map_markers_id',$marker_ids)->get();
                // $images = \App\Gallery::all();                
                foreach ($images as $row) {
                    $images_array[] = $row->image_path;
                }
            }else{
                return  json_encode(array('response' => array('code'=> 500 ,'status' => 'error','message' => 'Error , enter valid udid and markers' )));
            }


            /**
             * create temp images and resize it
             */
            $directory = "uploads/gallary/";
            $images = glob($directory . "*.jpg");
            foreach($images as $image)
            {   
                if (in_array(substr($image, 16), $images_array)) {
                    $this->resize($user_id,$image);
                }            
                // copy($image,'images/'.substr($image, 5));
            }

            /**
             * create horizintal grid images that contine 4 image horizintaly
             */
            $directory2 = "temp/".$user_id.'/';
            $images2 = glob($directory2 . "*.jpg");
            $counter = 0 ;
            $counter2 = -1 ;
            for ($i=0; $i < count($images2)-2; $i++) { 
                if ($i == 0) {
                    $counter2++;
                    $this->joinimageh($images2[$i],$images2[$i+1],'h/resultv'.$counter2.'.jpg');
                }elseif (($i%4)==3 ) {
                    $counter2++;
                    $this->joinimageh($images2[$i+1],$images2[$i+2],'h/resultv'.$counter2.'.jpg');
                }elseif ($i>3) {
                    $this->joinimageh('h/resultv'.$counter2.'.jpg',$images2[$i+2],'h/resultv'.$counter2.'.jpg');
                }else{
                    $this->joinimageh('h/resultv'.$counter2.'.jpg',$images2[$i+1],'h/resultv'.$counter2.'.jpg');
                }
            }

            /**
             * join horizintal grid images that contine all phots
             */
            $directory3 = "h/";
            $time = time();
            $images3 = glob($directory3 . "*.jpg");
            for ($i=0; $i < count($images3)-1; $i++) { 
                if ($i == 0) {
                    $this->joinimagev($images3[$i],$images3[$i+1],'share/'.$time.$user_id.'.jpg');
                }else{
                    $this->joinimagev('share/'.$time.$user_id.'.jpg',$images3[$i+1],'share/'.$time.$user_id.'.jpg');
                }
            }

            /**
             * return response with image url
             */
            return  json_encode(array('response' => array('code'=> 200 ,'status' => 'success','message'=>'success'  ), 'image' =>  $this->get_full_path('share',$time.$user_id.'.jpg')));
        }catch(\Exception $e){
             return  json_encode(array('response' => array('code'=> 500 ,'status' => 'error','message' => 'Error , some error :'.$e->getMessage() )));
        }
    }

    /**
     * add new user
     */
    public function add_new_user(){
        $udid = \Input::get('udid');
        if (!empty($udid)) {
            $user_record = \App\User::where('udid',$udid)->first();
            if (!empty($user_record->id) && intval($user_record->id) > 0 ) {
                return  json_encode(array('response' => array('code'=> 500 ,'status' => 'error','message' => 'Error , this user exists' )));
            }
            $user_record = \App\User::create([
                'udid' =>$udid,     
            ]);            
            return  json_encode(array('response' => array('code'=> 200 ,'message'=>'success' ,'status' => 'success' ), 'user' => $user_record));
        }else{
            return  json_encode(array('response' => array('code'=> 500 ,'status' => 'error','message' => 'Error , enter valid udid' )));
        }
    }


    /**
     * general function
     */

    /**
     * this function use to convert base64 to img
     */
    function base64_to_jpeg($base64_string) {

        $ext = substr($base64_string,11,3);
        $extntion = '';
        if( $ext == 'jpe' ){
            $extntion = 'jpg';
        }elseif($ext == 'jpg'){
            $extntion = 'jpg';
        }elseif($ext == 'png'){
            $extntion = 'png';
        }

        $output_file = md5(time()).'.'.$extntion;

        $ifp = fopen('uploads/gallary/'.$output_file, "wb"); 

        $data = explode(',', $base64_string);

        fwrite($ifp, base64_decode($data[1])); 
        fclose($ifp);     

        // return 'uploads/profile/'.time()."_".md5($uid).'.'.$extntion; 
        return $output_file; 
    }

    /**
     * this function to get user id form database using udid(android id or ios id)
     */
    public function get_user_id($udid)
    {
        $user = \App\User::where('udid',$udid)->first(); 
        if (!empty($user->id)) {            
            return  $user->id;
        }else{
            return 0;
        }
    }


    /**
     * this function to get user udid(android id or ios id) form database using id
     */
    public function get_user_udid($id)
    {
        $user = \App\User::whereId($id)->first(); 
        if (!empty($user->id)) {            
            return  $user->udid;
        }else{
            return 0;
        }
    }

    /**
     * this function return file with full path
     */
    public function get_full_path($folder,$file){
        return \URL::to($folder).'/'.$file;
    }


    /**
     * join horizantal images as 4 image in one
     */
    public function joinimageh($first,$scound,$destination){
         list($width_x, $height_x) = getimagesize($first);
         list($width_y, $height_y) = getimagesize($scound);
         // Create new image with desired dimensions
         $image = imagecreatetruecolor($width_x + $width_y, $height_x);
         // Load images and then copy to destination image
         $image_x = imagecreatefromjpeg($first);
         $image_y = imagecreatefromjpeg($scound);
         imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);
         imagecopy($image, $image_y, $width_x, 0, 0, 0, $width_y, $height_y);
         // Save the resulting image to disk (as JPEG)
        $filename_result = $destination;
         imagejpeg($image, $filename_result);
         // Clean up
         imagedestroy($image);
         imagedestroy($image_x);
         imagedestroy($image_y);
    }

    /**
     * join horizntail image in one grig image
     */
    public function joinimagev($first,$scound,$destination){
         list($width_x, $height_x) = getimagesize($first);
         list($width_y, $height_y) = getimagesize($scound);
         // Create new image with desired dimensions
         $image = imagecreatetruecolor($width_x, $height_x+$height_y );
         // Load images and then copy to destination image
         $image_x = imagecreatefromjpeg($first);
         $image_y = imagecreatefromjpeg($scound);
         imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);
         imagecopy($image, $image_y, 0, 300, 0, 0, $width_y, $height_y);
         // Save the resulting image to disk (as JPEG)
        $filename_result = $destination;
         imagejpeg($image, $filename_result);
         // Clean up
         imagedestroy($image);
         imagedestroy($image_x);
         imagedestroy($image_y);
    }

    /**
     * this function resize and create  temp images
     */
    public function resize($folder,$filename){ 
        // File and new size    
        // $newfilename="/temp/".$filename;
        $newfilename="temp/".$folder.'/'.substr($filename, 16);
        // Get new sizes
        list($width, $height) = getimagesize($filename);
        $newwidth = 300;
        $newheight = 300;
        // Load
        $thumb = imagecreatetruecolor($newwidth, $newheight);
        $source = imagecreatefromjpeg($filename);
        // Resize
        $res=imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        // Output
        imagejpeg($thumb,$newfilename);
    }


}
