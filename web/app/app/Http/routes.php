<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'web'], function () {

    Route::get('/', function(){
    	return View::make('welcome');
    });

    Route::get('/json/markers', 'APIController@getmarkers');
    Route::get('/json/gallary/images/{udid}/{markerid}', 'APIController@image_gallary');
    Route::post('/json/gallary/image', 'APIController@add_image_to_gallary');
    Route::post('/json/markers/add', 'APIController@add_new_marker');
    Route::post('/json/users/add', 'APIController@add_new_user');

    Route::post('/json/trip/share', 'APIController@shear_trip');
});
