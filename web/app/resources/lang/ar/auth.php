<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'تسجيل الدخول',
    'email' => 'البريد الإلكتروني',
    'username' => 'اسم المستخدم',
    'password' => 'كلمة المرور',
    'rememberme' => 'تذكرني',
    'forgotpassword' => 'نسيت كلمة المرور',

    'emailrequired' => 'البريد الإلكتروني مطلوب.',
    'passwordrequired' => 'كلمة المرور على الأقل 6 حروف.',
    'emailemail' => 'الرجاء إدخال بريد الكتروني بصيغة صحيحة.',
    'logininfoerror' => 'بيانات الدخول غير صحيحة.',


];
