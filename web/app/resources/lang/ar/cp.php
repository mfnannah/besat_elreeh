<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'controlpanel' => 'لوحة التحكم',
    'categories' => 'التصنيفات',
    'motivations' => 'المحتوى التفاعلي',
    'feelings' => 'المشاعر',
    'memos' => 'المذكرات',
    'rooms' => 'غرف الدردشة',
    'users' => 'المستخدمين',
    'settings' => 'الإعدادات',

    'addnew' => 'إضافة جديد',
    'edit' => 'تعديل',
    'required' => 'هذا الحقل مطلوب',
    'confirmdelete' => 'هل انت متاكد من عملية الحذف',
    'options' => 'الخيارات',
    'save' => 'حفظ',
    'status' => 'الحالة',
    'oprationsuccess' => 'تمت العملية بنجاح',
    'rownotfound' => 'العنصر غير موجود',

    'fullname' => 'الاسم كاملا',
    'username' => 'اسم المستخدم',
    'password' => 'كلمة المرور',
    'ifdontchngepass' => 'اذا كنت لا تريد تغير كلمة المرور اتركها فارغة',
    'email' => 'البريد الإلكتروني',
    'usertype' => 'نوع العضوية',

    'ADMIN' => 'مدير',
    'DOCTOR' => 'دكتور',
    'USER' => 'المستخدم',
    'ACTIVE' => 'مفعل',
    'INACTIVE' => 'غير مفعل',
    'MOTIVATION' => 'المحتوى التفاعلي',
    'ROOM' => 'غرف الدردشة',

    'PUBLIC' => 'عام',
    'PRIVATE' => 'خاص',
    'FOLLOWER' => 'الأصدقاء',

    'categoryname' => 'اسم التصنيف',
    'categoryadmin' => 'مدير التصنيف',
    'categorytype' => 'نوع التصنيف',

    'feelingname' => 'اسم الشعور',

    'motivationname' => 'العنوان',
    'motivationcontent' => 'المحتوى',
    'motivationcategory' => 'التصنيف',

    'followers' => 'الأصدقاء',
    'permissions' => 'الصلاحيات',

    'degree' => 'ألدرجة',
    'text' => 'نص المذكرة',
    'privacy' => 'الخصوصية',

    /********* validation **********/
    'namerequired' => 'الاسم مطلوب',
    'namemin' => 'الاسم على الأقل 3 حروف',
    'typerequired' => 'النوع مطلوب',
    'statusrequired' => 'الحالة مطلوبة',

    'titlerequired' => 'العنوان مطلوب',
    'titlemin' => 'العنوان على الأقل 3 حروف',
    'contentrequired' => 'المحتوى مطلوب',
    'contentmin' => 'المحتوى على الأقل 10 حروف',
    'category_idrequired' => 'التصنيف مطلوب',

    'usernamerequired' => 'اسم المستخدم مطلوب',
    'usernamemin' => 'اسم المستخدم على الأقل 3 حروف',
    'usernameunique' => 'اسم المستخدم هذا موجود مسبقا',
    'emailrequired' => 'كلمة المرور مطلوب',
    'emailemail' => 'كلمة المرور يجب ان يكون بصيغة صحيحة',
    'emailunique' => 'هذا البريد الالكتروني موجود مسبقا',
    'passwordrequired' => 'كلمة المرور مطلوبة',
    'passwordmin' => 'كلمة المرور على الأقل 6 حروف',



];
