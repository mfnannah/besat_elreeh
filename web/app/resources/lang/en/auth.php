<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Login',
    'email' => 'E-mail',
    'username' => 'Username',
    'password' => 'Password',
    'rememberme' => 'Remember me',
    'forgotpassword' => 'Forgot Password',

    'emailrequired' => 'E-mail is Required.',
    'passwordrequired' => 'Password is Required.',
    'emailemail' => 'E-mail mast be valid.',
    'logininfoerror' => 'Enter valid data.',

];
