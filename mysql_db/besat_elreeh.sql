-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 07, 2016 at 01:32 AM
-- Server version: 5.5.45-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ss_besat_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `countries_categoies`
--

CREATE TABLE IF NOT EXISTS `countries_categoies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `map_markers_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `map_markers_id`, `user_id`, `image_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'aaskks.jpg', '2016-05-06 11:52:04', NULL, NULL),
(2, 1, 1, 'f1d098fc18489464be305d6cebedd4df.jpg', '2016-05-06 20:38:56', '2016-05-06 20:38:56', NULL),
(3, 1, 1, '2d51fee95027fbee9dbd7f24d57f99f0.jpg', '2016-05-06 21:03:28', '2016-05-06 21:03:28', NULL),
(4, 8, 6, '585de899b0057bdae2fadc0cd92a9d6b.jpg', '2016-05-07 11:00:40', '2016-05-07 11:00:40', NULL),
(5, 8, 6, '9a8076d47117dd823adadd3309f549a6.jpg', '2016-05-07 11:00:55', '2016-05-07 11:00:55', NULL),
(6, 8, 6, 'f94fb0088e3e4b520f937f66db5913be.jpg', '2016-05-07 11:03:01', '2016-05-07 11:03:01', NULL),
(7, 8, 6, '20d1d01f505b8a56ac2e4667d309679a.jpg', '2016-05-07 11:08:17', '2016-05-07 11:08:17', NULL),
(8, 8, 6, '647c12fbf4edc0185689f6f2b3048083.jpg', '2016-05-07 11:08:31', '2016-05-07 11:08:31', NULL),
(9, 8, 6, 'de889708464d78123532dfe091ea13ec.jpg', '2016-05-07 11:10:50', '2016-05-07 11:10:50', NULL),
(10, 8, 6, '28906fb77cf3c4c3d8fa64f879bf51d1.jpg', '2016-05-07 11:11:00', '2016-05-07 11:11:00', NULL),
(11, 10, 6, 'b51d6be41041dab583067dae7d150713.jpg', '2016-05-07 11:11:53', '2016-05-07 11:11:53', NULL),
(12, 10, 6, 'c1ef95ae6aade75ec47ae3e131772a72.jpg', '2016-05-07 13:06:38', '2016-05-07 13:06:38', NULL),
(13, 12, 6, '5adadfc5310cd10cb9ffa6dff8b8fb39.jpg', '2016-05-07 13:56:43', '2016-05-07 13:56:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `map_markers`
--

CREATE TABLE IF NOT EXISTS `map_markers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `rate` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `map_markers`
--

INSERT INTO `map_markers` (`id`, `latitude`, `longitude`, `name`, `rate`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '25.0025', '50.56', 'dawha', 3.5, '2016-05-06 11:41:18', NULL, NULL),
(2, '30.565', '49.265', 'gaza', 4.6, '2016-05-06 11:41:18', NULL, NULL),
(3, '23.25', '52.3', 'test point', 3.5, '2016-05-06 20:52:42', '2016-05-06 20:52:42', NULL),
(4, '31.51715573686233', '34.44414362311363', 'location', 5, '2016-05-07 05:04:13', '2016-05-07 05:04:13', NULL),
(5, '31.512734608332554', '34.44844387471676', 'لار', 10, '2016-05-07 06:43:18', '2016-05-07 06:43:18', NULL),
(6, '31.514715967465314', '34.44226339459419', 'وثب', 10, '2016-05-07 06:45:33', '2016-05-07 06:45:33', NULL),
(7, '31.51007777189706', '34.44681208580732', 'good', 8, '2016-05-07 06:54:24', '2016-05-07 06:54:24', NULL),
(8, '31.510696039770856', '34.44490972906351', 'ggg', 5, '2016-05-07 07:01:00', '2016-05-07 07:01:00', NULL),
(9, '31.513756734216003', '34.44231268018484', 'jvc', 6, '2016-05-07 07:23:08', '2016-05-07 07:23:08', NULL),
(10, '31.508186355601026', '34.44524735212326', 'hggg', 9, '2016-05-07 07:23:36', '2016-05-07 07:23:36', NULL),
(11, '31.510315303866374', '34.448771104216576', 'اثب', 9, '2016-05-07 13:03:49', '2016-05-07 13:03:49', NULL),
(12, '31.514422710669503', '34.4466819986701', 'مكاني', 5, '2016-05-07 13:42:34', '2016-05-07 13:42:34', NULL),
(13, '31.511506384674043', '34.44170247763395', 'location', 8, '2016-05-07 13:48:24', '2016-05-07 13:48:24', NULL),
(14, '31.509288852720992', '34.44250579923391', 'my location', 8, '2016-05-07 13:57:19', '2016-05-07 13:57:19', NULL),
(15, '31.51068060456163', '34.44014009088278', 'اسم', 6, '2016-05-07 14:09:45', '2016-05-07 14:09:45', NULL),
(16, '31.513001002397232', '34.44766268134117', 'ااا', 5, '2016-05-07 14:18:44', '2016-05-07 14:18:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `travels`
--

CREATE TABLE IF NOT EXISTS `travels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `qrcode` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `travels_map_markers`
--

CREATE TABLE IF NOT EXISTS `travels_map_markers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `travel_id` int(11) NOT NULL,
  `map_markers_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `travels_services`
--

CREATE TABLE IF NOT EXISTS `travels_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `travels_id` int(11) NOT NULL,
  `services_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `udid` varchar(255) DEFAULT NULL,
  `type` enum('USER','GUEST') NOT NULL DEFAULT 'GUEST',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `udid`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, NULL, NULL, '1112365455', 'GUEST', '2016-05-06 11:51:19', NULL, NULL),
(3, NULL, NULL, NULL, '123654', 'GUEST', '2016-05-06 21:57:37', '2016-05-06 21:57:37', NULL),
(6, NULL, NULL, NULL, '26a61a699c7c0c2a', 'GUEST', '2016-05-07 10:28:17', '2016-05-07 10:28:17', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
